<?php
$main_title = get_sub_field('main_title');
?>

<div class="issuesGrid">
    <div class="issuesGrid__container">
        <div class="issuesGrid__title"><?php echo $main_title; ?></div>
        <div class="issuesGrid__box">
            <?php
            while( have_rows('grid_element') ) {
            the_row();
            $grid_title = get_sub_field('grid_title');
            $grid_description = get_sub_field('grid_description');
            ?>
            <div class="issuesGridItemNew">
                <div class="issuesGridItemNew__name"><?php echo $grid_title; ?></div>
                <div class="issuesGridItemNew__icon">
                    <img loading="lazy" class="issuesGridItemNew__open" src="<?php echo TEMP_URI; ?>/assets/images/plus.svg" alt="Open">
                    <img loading="lazy" class="issuesGridItemNew__close" src="<?php echo TEMP_URI; ?>/assets/images/minus.svg" alt="Close">
                </div>
                <div class="issuesGridItemNew__text"><p><?php echo $grid_description; ?></p></div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="issuesGrid__decoratorTop"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" alt="Decorator desktop"></div>
    <div class="issuesGrid__decoratorBottom"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green-m.svg" alt="Decorator mobile"></div>
</div>