<?php
$map_title = get_sub_field('map_title');
$map_description = get_sub_field('map_description');
$map_down_description = get_sub_field('map_down_description');
$map_stats_description = get_sub_field('map_stats_description');
$map_image = get_sub_field('map_image');
$map_image = wp_get_attachment_image_url($map_image['ID'],'Map');
$add_grid = get_sub_field('add_grid');
?>


<div class="mapSection">
    <div class="mapSection__container">
        <div class="mapSection__decorator"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-dark-green-m.svg" alt="Decorator"></div>
        <div class="mapSection__title"><?php echo $map_title; ?></div>
        <div class="mapSection__subtitle"><?php echo $map_description; ?></div>
        <?php if(!empty($map_image)){ ?>
        <div class="mapSection__mapPlace"><img loading="lazy" src="<?php echo $map_image; ?>" alt="Map image"></div>
        <?php } ?>
        <div class="mapSection__text"><?php echo $map_down_description; ?></div>
        <?php if(((int)$add_grid[0] === 1)){ ?>
        <div class="mapSection__stats">
            <?php
            while (have_rows('repeater_icon')) {
                the_row();
                $map_icon = get_sub_field('map_icon');
                $map_icon = wp_get_attachment_image_url($map_icon['ID'],'Icon');
                $text_icon = get_sub_field('text_icon');
            ?>
            <div class="mapSection__item">
                <div class="mapSection__icon"><img loading="lazy" src="<?php echo $map_icon ?>" alt="Icon"></div>
                <div class="mapSection__itemText"><?php echo $text_icon ?></div>
            </div>
            <?php } ?>
        </div>
            <?php if(!empty($map_stats_description)){ ?>
                <div class="mapSection__text"><?php echo $map_stats_description; ?></div>
            <?php } ?>
        <?php } ?>
    </div>
</div>