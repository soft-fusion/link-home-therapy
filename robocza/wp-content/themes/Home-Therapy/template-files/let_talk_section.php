<?php
$title_section = get_sub_field('title_section');
$button_text = get_sub_field('button_text');
$button_url = get_sub_field('button_url');
?>


<div class="letsTalk">
    <div class="letsTalk__container">
        <div class="letsTalk__textBox">
            <div class="letsTalk__title"><?php echo $title_section; ?></div>
            <a href="<?php echo $button_url; ?>" class="customButton"><?php echo $button_text; ?></a>
        </div>
    </div>
    <div class="letsTalk__backToTop"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/backToTop.svg" alt="Back top">
        <div class="letsTalk__backText">Back to top</div>
    </div>
</div>