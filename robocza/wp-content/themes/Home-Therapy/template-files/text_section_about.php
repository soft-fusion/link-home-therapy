<?php
$description_section = get_sub_field('description_section');
?>

<div class="textSection">
    <div class="textSection__container">
        <picture class="textSection__dashes">
            <source media="(min-width: 768px)" srcset="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" type="image/png">
            <source srcset="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-dark-green-m.svg" type="image/png"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" alt="Decorator">
        </picture>
        <div class="textSection__green"></div>
        <div class="textSection__right -wider">
            <div class="textSection__text"><img loading="lazy" class="textSection__square -only-mobile" src="<?php echo TEMP_URI; ?>/assets/images/categoryDecorator.svg" alt="Decorator">
                <?php echo $description_section; ?>
            </div>
        </div>
    </div>
</div>
