<?php
$testimonial_title = get_sub_field('testimonial_title');
$testimonials_description = get_sub_field('testimonials_description');
$testimonial_section = get_sub_field('testimonial_section');

?>

<div class="testimonialsSection">
    <div class="testimonialsSection__container">
        <div class="testimonialsSection__title"><?php echo $testimonial_title; ?></div>
        <div class="testimonialsSection__text"><?php echo $testimonials_description; ?></div>
        <div class="testimonialsSection__testimonials">
            <div class="testimonialsSection__slider">
                <?php
                foreach ($testimonial_section as $arg) {
                    $my_id = $arg['select_post'];
                    $id = $my_id->ID;
                    $from = get_field('from_text',$id);
                ?>
                <div class="testimonialsItem">
                    <div class="testimonialsItem__text"><?php echo $my_id->post_content ?></div>
                    <div class="testimonialsItem__author"><?php echo get_the_title($id) ?></div>
                    <div class="testimonialsItem__from"><?php echo $from ?></div>
                </div>
                <?php } ?>
            </div>
            <div class="testimonialsSection__prev"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/slickArrowLeftWhite.svg" alt="Previous"></div>
            <div class="testimonialsSection__next"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/slickArrowRightWhite.svg" alt="Next"></div>
        </div>
    </div>
</div>