<?php
$button_section_title = get_sub_field('button_section_title');
$button_text = get_sub_field('button_text');
$button_url = get_sub_field('button_url');
?>

<div class="buttonSection">
    <div class="buttonSection__container">
        <div class="buttonSection__title"><?php echo $button_section_title; ?></div>
        <a href="<?php echo $button_url; ?>"><button class="customButton"><?php echo $button_text; ?></button></a>
    </div>
    <div class="buttonSection__decorator"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" alt="decorator"></div>
</div>