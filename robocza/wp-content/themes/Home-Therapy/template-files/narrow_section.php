<?php
$narrow_section = get_sub_field('narrow_section');
$narrow_description = get_sub_field('narrow_description');
$image_decktop = get_sub_field('image_decktop');
$image_decktop = wp_get_attachment_image_url($image_decktop['ID'], 'Narrow_desktop');
$image_mobile = get_sub_field('image_mobile');
$image_mobile = wp_get_attachment_image_url($image_mobile['ID'], 'Narrow_mobile');
?>

<div class="narrowText">
    <div class="narrowText__container">
        <div class="narrowText__mobile"> <img loading="lazy" class="narrowText__image" src="<?php echo $image_mobile; ?>" alt="Image mobile">
            <div class="narrowText__title -mobile"><?php echo $narrow_section; ?></div>
        </div>
        <div class="narrowText__left"><img loading="lazy" class="narrowText__square" src="<?php echo TEMP_URI; ?>/assets/images/decorator-banner-square.svg" alt="Decorator">
            <div class="narrowText__title -desktop"><?php echo $narrow_section; ?></div>
            <div class="narrowText__text"><?php echo $narrow_description; ?></div>
        </div>
        <div class="narrowText__right">
            <img loading="lazy" class="narrowText__photo" src="<?php echo $image_decktop; ?>" alt="Image desktop">
            <img loading="lazy" class="narrowText__square" src="<?php echo TEMP_URI; ?>/assets/images/decorator-banner-square.svg" alt="Decorator"></div>
        <div class="narrowText__backToTop"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/backToTop.svg" alt="Back top">
            <div class="narrowText__backText">Back to top</div>
        </div>
    </div>
</div>


