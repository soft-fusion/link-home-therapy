<?php
$banner_option = get_sub_field('banner_options');
$banner_tag = get_sub_field('banner_tag');
$banner_title = get_sub_field('banner_title');
$page_banner = get_sub_field('page_banner');
$mobile_style = get_sub_field('mobile_style');
$banner_description = get_sub_field('banner_description');
$banner_image_desktop = get_sub_field('banner_image_desktop');
$banner_image_desktop = wp_get_attachment_image_url($banner_image_desktop['ID'],'');
$banner_image_mobile = get_sub_field('banner_image_mobile');
$banner_image_mobile = wp_get_attachment_image_url($banner_image_mobile['ID'], 'Banner_mobile');
switch ((int)$page_banner)
{
    case 1:
        $banner_image_desktop = get_sub_field('banner_image_desktop');
        $banner_image_desktop = wp_get_attachment_image_url($banner_image_desktop['ID'],'Banner_home');
        $decorator_image = get_sub_field('decorator_image');
        $decorator_image = wp_get_attachment_image_url($decorator_image['ID'], 'Decorator');
        ?>
        <div class="banner">
            <div class="banner__container">
                <picture>
                    <source media="(min-width: 481px)" srcset="<?php echo $banner_image_desktop; ?>" type="image/png">
                    <source srcset="<?php echo $banner_image_mobile; ?>" type="image/png">
                    <img loading="lazy" class="banner__image" src="<?php echo $banner_image_desktop; ?>" alt="page banner">
                </picture>
                <picture>
                    <source media="(min-width: 768px)" srcset="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" type="image/png">
                    <source srcset="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green-m.svg" type="image/png">
                    <img loading="lazy" class="banner__dashes -lower-on-mobile" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" alt="decorator">
                </picture>
                <div class="banner__whiteAbsoluteBox"><img loading="lazy" class="banner__seal" src="<?php echo $decorator_image; ?>" alt="animation decorator">
                    <div class="banner__title"><?php echo $banner_title; ?></div>
                    <div class="banner__text"><?php echo $banner_description; ?></div>
                    <?php
                    $add_button = get_sub_field('add_button');
                    $add_button = (int)$add_button[0];
                    if($add_button === 1)
                            {
                                $button_text = get_sub_field('button_text');
                                $button_url = get_sub_field('button_url');
                            ?>
                                <a href="<?php echo $button_url; ?>" class="banner__button" title="<?php echo $button_text; ?>"><?php echo $button_text; ?></a>
                        <?php }?>
                </div>
            </div>
        </div>
        <?php
        break;
    case 2:
        $banner_image_desktop = get_sub_field('banner_image_desktop');
        $banner_image_desktop = wp_get_attachment_image_url($banner_image_desktop['ID'],'Banner_about');
        ?>
        <div class="banner -narrower">
            <div class="banner__container">
                <picture>
                    <source media="(min-width: 481px)" srcset="<?php echo $banner_image_desktop; ?>" type="image/png">
                    <source srcset="<?php echo $banner_image_mobile; ?>" type="image/png">
                    <img loading="lazy" class="banner__image" src="<?php echo $banner_image_desktop; ?>" alt="banner about">
                </picture>
                <img loading="lazy" class="banner__dashes -only-mobile" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-dark-green-m.svg" alt=""><img loading="lazy" class="banner__square" src="<?php echo TEMP_URI; ?>/assets/images/decorator-banner-square.svg" alt="decorator">
                <div class="banner__transparentAbsoluteBox">
                    <div class="banner__tag"><?php echo $banner_tag ?></div>
                    <div class="banner__title -font-900"><?php echo $banner_title; ?></div>
                </div>
            </div>
        </div>
        <?php
        break;
    case 3:
        $banner_image_desktop = get_sub_field('banner_image_desktop');
        $banner_image_desktop = wp_get_attachment_image_url($banner_image_desktop['ID'],'Banner_what');
        ?>
        <div class="banner -smaller-photo">
            <div class="banner__container">
                <picture>
                    <source media="(min-width: 481px)" srcset="<?php echo $banner_image_desktop; ?>" type="image/png">
                    <source srcset="<?php echo $banner_image_mobile; ?>" type="image/png">
                    <img loading="lazy" class="banner__image" src="<?php echo $banner_image_desktop; ?>" alt="banner what we do">
                </picture>
                <img loading="lazy" class="banner__dashes -only-desktop" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" alt="decorator">
                <div class="banner__transparentAbsoluteBoxLeft">
                    <div class="banner__title -font-900"><?php echo $banner_title; ?></div>
                </div>
            </div>
        </div>
        <?php
        break;
    case 4:
        $banner_image_desktop = get_sub_field('banner_image_desktop');
        $banner_image_desktop = wp_get_attachment_image_url($banner_image_desktop['ID'],'Banner_partners');
        ?>

        <div class="banner -more-narrower">
            <div class="banner__container">
                <picture>
                    <source media="(min-width: 481px)" srcset="<?php echo $banner_image_desktop; ?>" type="image/png">
                    <source srcset="<?php echo $banner_image_mobile; ?>" type="image/png">
                    <img loading="lazy" class="banner__image" src="<?php echo $banner_image_desktop; ?>" alt="banner partners">
                </picture>
                <img loading="lazy" class="banner__dashes -only-mobile" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-dark-green-m.svg" alt="decorator mobile">
                <div class="banner__greenAbsoluteBox <?php if(((int)$mobile_style[0] === 1)){ ?>-absolute-bottom-on-mobile<?php } ?>">
                    <div class="banner__title"><?php echo $banner_title; ?></div><img loading="lazy" class="banner__square" src="<?php echo TEMP_URI; ?>/assets/images/decorator-banner-square.svg" alt="decorator">
                </div>
            </div>
        </div>
        <?php
        break;
    case 5:
        $banner_image_desktop = get_sub_field('banner_image_desktop');
        $banner_image_desktop = wp_get_attachment_image_url($banner_image_desktop['ID'],'Banner_contact');
        ?>
        <div class="banner -smaller-photo -without-shadow -without-margin-bottom">
            <div class="banner__container">
                <picture>
                    <source media="(min-width: 481px)" srcset="<?php echo $banner_image_desktop; ?>" type="image/png">
                    <source srcset="<?php echo $banner_image_mobile; ?>" type="image/png">
                    <img loading="lazy" class="banner__image" src="<?php echo $banner_image_desktop; ?>" alt="banner contact">
                </picture>
            </div>
        </div>
        <?php
        break;
}
?>
