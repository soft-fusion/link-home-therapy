<?php
$team_tag = get_sub_field('team_tag');
$team_title = get_sub_field('team_title');
$team_section = get_sub_field('team_section');
?>


<div class="peopleGrid">
    <div class="peopleGrid__container">
        <div class="peopleGrid__title"><?php echo $team_tag; ?></div>
        <div class="peopleGrid__text"><?php echo $team_title; ?></div>
        <div class="peopleGrid__slider">
            <div class="peopleGrid__box">
                <?php
                foreach ($team_section as $arg) {
                $my_id = $arg['select_leadership'];
                $id = $my_id->ID;
                $short_description = get_field('short_description',$id);
                $position = get_field('position',$id);
                ?>
                <div class="peopleGridItem">
                    <div class="peopleGridItem__photo"><img loading="lazy" src="<?php echo get_the_post_thumbnail_url($id) ?>" alt="<?php echo get_the_title($id) ?>"></div>
                    <div class="peopleGridItem__name"><?php echo get_the_title($id) ?></div>
                    <div class="peopleGridItem__position"><?php echo $position; ?></div>
                    <div class="peopleGridItem__text"><?php echo $short_description; ?></div>
                </div>
                <?php } ?>
            </div>
            <div class="peopleGrid__prev"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/slickArrowLeftGreen.svg" alt="Previous"></div>
            <div class="peopleGrid__next"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/slickArrowRightGreen.svg" alt="Next"></div>
        </div>
    </div>
</div>