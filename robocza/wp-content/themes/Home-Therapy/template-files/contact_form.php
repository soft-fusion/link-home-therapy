<?php
$title_form = get_sub_field('title_form');
$form_description = get_sub_field('form_description');
$email_sent = get_sub_field('email_sents');
$call_number = get_sub_field('call_number');
$email = get_sub_field('email');
$whatapp = get_sub_field('whatapp');
$fax = get_sub_field('fax');
$bottom_text = get_sub_field('bottom_text');
$add_description = get_sub_field('add_description');
$add_file_button = get_sub_field('add_file_button');
$page_contact = get_sub_field('page_contact');
$bottom_image = get_sub_field('bottom_image');
$bottom_image = wp_get_attachment_image_url($bottom_image['ID'],'Contact');
?>

<div class="contactSection <?php if((int)$page_contact[0] != 1){ echo '-partner';} ?>">
    <div class="contactSection__bg"></div>
    <div class="contactSection__container">
        <div class="contactSection__box">
            <div class="contactSection__decorator -desktop"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" alt="Decorator desktop"></div>
            <div class="contactSection__decorator -mobile"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-<?php if((int)$page_contact[0] != 1){echo "dark-";}?>green-m.svg" alt="Decorator mobile">
            </div>
            <div class="contactSection__left">
                <div class="contactSection__title"><?php echo $title_form; ?></div>
                <?php if(((int)$add_description[0] === 1)){ ?>
                <div class="contactSection__text">
                    <?php echo $form_description; ?>
                </div>
                <?php } ?>
                <div class="contactSection__contactData -desktop">
                    <div class="contactSection__data">
                        <div class="contactSection__type">Call</div>
                        <div class="contactSection__value"><a href="tel:<?php echo $call_number; ?>"><?php echo $call_number; ?> </a></div>
                    </div>
                    <div class="contactSection__data">
                        <div class="contactSection__type">Email</div>
                        <div class="contactSection__value"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
                    </div>
                    <div class="contactSection__data">
                        <div class="contactSection__type">Whatsapp</div>
                        <div class="contactSection__value"><?php echo $whatapp; ?></div>
                    </div>
                    <div class="contactSection__data">
                        <div class="contactSection__type">Fax</div>
                        <div class="contactSection__value"><?php echo $fax; ?></div>
                    </div>
                </div>
                <?php if(((int)$add_description[0] === 1)){ ?>
                <div class="contactSection__bottomText"><?php echo $bottom_text; ?></div>
                <?php } ?>
            </div>
            <div class="contactSection__right">
                <div class="contactSection__form">
                    <div class="partnerForm">
                        <div id="refresh_email">
                            <div id="usermessage"></div>
                        </div>
                        <div id="form_refresh">
                        <div id="send_refresh">
                            <form class="partnerForm__form" method="post" id="contact_form">
                                <div class="partnerForm__inputPlace">
                                    <input class="partnerForm__input" type="text" placeholder="Name" id="name" name="name">
                                    <input class="partnerForm__input" type="email" placeholder="Email" id="email" name="email">
                                </div>
                                <div class="partnerForm__phone">
                                    <input class="partnerForm__input" type="text" placeholder="Phone" id="phone" name="phone">
                                </div>
                                <textarea class="partnerForm__textarea" placeholder="Message" id="message" name="message"></textarea>
                                <input type="hidden" value="<?php echo $email_sent ?>" id="sent_email" name="sent_email">
                                    <?php if(((int)$add_file_button[0] === 1)){ ?>
                                <div class="partnerForm__buttons">
                                    <input type="hidden" value="file" id="type_mail" name="type_mail">
                                    <input class="partnerForm__upload" type="file" id="send_file" name="send_file">
                                    <label class="partnerForm__button -outline" for="send_file">Attach Resume</label>

                                    <button class="partnerForm__button" type="submit" id="button_send">Send</button>
                                </div>
                                    <?php }
                                    else
                                    {
                                        ?>
                                            <input type="hidden" value="text" id="type_mail" name="type_mail">
                                        <button class="contact__button" type="submit" id="button_send">Send</button>

                                    <?php
                                    }
                                    ?>

                            </form>
                            <div class="partnerForm__fileMessage" id="file-message"></div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="contactSection__contactData -mobile">
                    <div class="contactSection__data">
                        <div class="contactSection__type">Call</div>
                        <div class="contactSection__value"><a href="tel:<?php echo $call_number; ?>"><?php echo $call_number; ?> </a></div>
                    </div>
                    <div class="contactSection__data">
                        <div class="contactSection__type">Email</div>
                        <div class="contactSection__value"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
                    </div>
                    <div class="contactSection__data">
                        <div class="contactSection__type">Whatsapp</div>
                        <div class="contactSection__value"><?php echo $whatapp; ?></div>
                    </div>
                    <div class="contactSection__data">
                        <div class="contactSection__type">Fax</div>
                        <div class="contactSection__value"><?php echo $fax; ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contactSection__bottomImage"><img loading="lazy" src="<?php echo $bottom_image; ?>" alt="Contact Form"></div>
        <?php if((int)$page_contact[0] != 1){
            ?>
            <div class="contactSection__bottomDecorator"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green.svg" alt="Decorator contact form"></div>
            <?php
        } ?>
         </div>
</div>