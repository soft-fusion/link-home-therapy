<?php
$tag_section = get_sub_field('tag_section');
$title_section = get_sub_field('title_section');
$description_section = get_sub_field('description_section',false);
$add_tag_and_title = get_sub_field('add_tag_and_title',false);
$type_segment = get_sub_field('type_segment',false);
$add_icon = get_sub_field('add_icon');
$margin_section = get_sub_field('margin_section');



switch ((int)$type_segment)
{
    case 1:
        ?>
        <div class="textSection">
            <div class="textSection__container">
                <img loading="lazy" class="textSection__dashes -only-mobile" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-dark-green-m.svg" alt="Decorator">
                <div class="textSection__left">
                    <?php if((int)$add_tag_and_title[0] === 1){ ?>
                    <div class="textSection__tag"><?php echo $tag_section; ?>
                        <img loading="lazy" class="textSection__square" src="<?php echo TEMP_URI; ?>/assets/images/decorator-banner-square.svg" alt="Decorator">
                    </div>
                    <div class="textSection__title"><?php echo $title_section; ?></div>
                <?php } ?>
                </div>
                <div class="textSection__right">
                    <div class="textSection__text">
                        <?php echo $description_section; ?>
                    </div>
                </div>
            </div>
        </div>
<?php
        break;
    case 2:
        ?>
        <div class="textSection  <?php if((int)$margin_section === 1){echo '-more-padding-bottom-partner';}elseif((int)$margin_section === 2){echo '-more-padding-bottom-join';} ?>">
            <div class="textSection__container">
                <div class="textSection__transparent"></div>
                <div class="textSection__right">
                    <div class="textSection__text">
                        <?php echo $description_section; ?></div>
                </div>
            </div>
            <?php if(((int)$add_icon[0] === 1)){ ?>
                <div class="textSection__icons">
                    <?php
                    while (have_rows('repeater_icon')) {
                        the_row();
                        $icon = get_sub_field('icon');
                        $icon = wp_get_attachment_image_url($icon['ID'], 'Icon2');
                        $text_icon = get_sub_field('text_icon');
                        ?>
                            <div class="textSection__item">
                                <div class="textSection__icon"><img loading="lazy" src="<?php echo $icon; ?>" alt="Icon"></div>
                                <div class="textSection__itemText"><?php echo $text_icon ?></div>
                            </div>
                        <?php } ?>
                </div>
            <?php } ?>
        </div>
        <?php
        break;
}