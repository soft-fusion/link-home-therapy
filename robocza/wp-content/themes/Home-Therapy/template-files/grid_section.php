<?php
$grid_section_title = get_sub_field('grid_section_title');
$image_text = get_sub_field('image_text');

$args = sf_get_posts(array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => $image_text,
    'order' => 'DESC',
));
?>
<div class="categoryGrid">
    <div class="categoryGrid__container">
        <div class="categoryGrid__title"><?php echo $grid_section_title; ?></div>
        <div class="categoryGrid__slider">
            <div class="categoryGrid__box">

                <?php
                foreach ($args as $arg)
                {
                    $image_text = get_field('image_text',$arg->ID);
                    $short_description = get_field('short_description',$arg->ID);
                    $desktop_image = get_field('desktop_image',$arg->ID);
                    $desktop_image = wp_get_attachment_image_url($desktop_image['ID'],'Post_desktop');
                    $mobile_image = get_field('mobile_image',$arg->ID);
                    $mobile_image = wp_get_attachment_image_url($mobile_image['ID'],'Post_mobile');
                ?>
                <div class="categoryGridItem">
                    <div class="categoryGridItem__imageBox">
                        <div class="categoryGridItem__image -mobile">
                            <img loading="lazy" src="<?php echo $mobile_image; ?>" alt="Physical Theory">
                        </div>
                        <div class="categoryGridItem__image -desktop">
                            <img loading="lazy" src="<?php echo $desktop_image; ?>" alt="Physical Theory">
                        </div>
                        <div class="categoryGridItem__bottom">
                            <div class="categoryGridItem__decorator"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/categoryDecorator.svg" alt="Decorator"></div>
                            <div class="categoryGridItem__category"><?php echo $image_text ?></div>
                        </div>
                    </div>
                    <div class="categoryGridItem__textBox">
                        <div class="categoryGridItem__title"><?php echo get_the_title($arg->ID); ?></div>
                        <div class="categoryGridItem__text"><?php echo $short_description; ?></div>
                    </div>
                </div>
                <?php } ?>

            </div>
            <div class="categoryGrid__prev"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/slickArrowLeftGreen.svg" alt="Previous"></div>
            <div class="categoryGrid__next"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/slickArrowRightGreen.svg" alt="Next"></div>
        </div>
    </div>
</div>