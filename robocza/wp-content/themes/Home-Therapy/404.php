
<?php
/**
 * Template name: 404
 */
get_header();
?>
    <div class="page404">
        <div class="page404__content">
            <div class="page404__title"><?php echo "There was an error: 404 - the page with this address does not exist."?></div>
            <div class="page404__textBox">
                <div class="page404__text">
                    <h2><?php echo "Try the following operations:";?></h2>
                    <ul>
                        <li> <?php echo 'Try to open the <a href="/"> home page </a> of the website'?></li>
                        <li> <?php echo "Try to connect to the site from a different computer"?></li>
                        <li> <?php echo "If the problem persists, you can contact the website owner"?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>