<?php
get_header();

the_post();
global $post;
if(have_rows('fields')) {

    $close_div = 0;
    while (have_rows('fields')) {
        the_row();
        $block = get_row_layout();
        include TEMP_VAR . '/template-files/' . $block . '.php';

    }
}
?>
<?php
get_footer();