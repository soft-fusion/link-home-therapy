<?php


add_image_size( 'Decorator', 98 , 98, true );
add_image_size( 'Contact', 1140 , 400, true );
add_image_size( 'Social', 22 , 21, true );
add_image_size( 'Banner_mobile', 414 , 576, true );
add_image_size( 'Banner_home', 1440 , 780, true );
add_image_size( 'Banner_about', 1140 , 511, true );
add_image_size( 'Banner_what', 1140 , 518, true );
add_image_size( 'Banner_partners', 1011 , 520, true );
add_image_size( 'Banner_contact', 1440 , 613, true );
add_image_size( 'Post_desktop', 366 , 260, true );
add_image_size( 'Post_mobile', 789 , 348, true );
add_image_size( 'Map', 796 , 513, true );
add_image_size( 'Icon', 86 , 86, true );
add_image_size( 'Icon2', 83 , 83, true );
add_image_size( 'Narrow_desktop', 439 , 588, true );
add_image_size( 'Narrow_mobile', 768 , 390, true );


