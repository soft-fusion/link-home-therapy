<?php
add_action('wp_ajax_contact_form', 'contact_form');
add_action('wp_ajax_nopriv_contact_form', 'contact_form');

function contact_form()
{

//    $email = get_field('email_address', 'option');
    $name = $_POST['name'];
    $email = $_POST['sent_email'];
    $email_form = $_POST['email'];
    $message = $_POST['message'];
    $phone = $_POST['phone'];
    $description = 'Name: '.$name.'<br> Email: '.$email_form.'<br> Phone: '.$phone .'<br> Message: '.$message;
    $type_mail = $_POST['type_mail'];
    if($type_mail === 'file') {
        $file = $_FILES['send_file'];
        if ($file['type'] === 'application/pdf' || $file['type'] === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || $file['type'] === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            $title = get_field('cv_mail_title', 'option');
            $upload_overrides = array('test_form' => false);
            $movefile = wp_handle_upload($file, $upload_overrides);
            $file = array($movefile['file']);
            sf_send_email($email, $description, $title, $file);
        }
    }
    elseif($type_mail === 'text')
    {
        $file = 0;
        $title = get_field('contact_mail_title', 'option');
        sf_send_email($email, $description, $title, $file);
    }
}