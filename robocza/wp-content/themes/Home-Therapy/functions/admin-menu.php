<?php
if(! defined( 'ABSPATH' )) return;

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

register_nav_menus(array(
    'top_menu' => __("Top Menu"),
    'footer_menu' => __("Footer Menu"),
));


function remove_wp_version_rss() { return ''; }
add_filter('the_generator', 'remove_wp_version_rss');
function vc_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 99999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 99999 );

function custom_menu_page_removing() {
}
add_action('admin_menu', 'custom_menu_page_removing');

function filter_url_img($image)
{
    $image[0] = utf8_uri_encode($image[0]);
    return $image;
}

add_filter('wp_get_attachment_image_src', 'filter_url_img' );


add_action('wp_head','add_ajax_url');
function add_ajax_url()
{
    ?>
    <script type="text/javascript">
        const ajax_link = '<?php echo admin_url("admin-ajax.php"); ?>';
        const ajax_link2 = '<?php echo admin_url("admin-ajax.php?action=contact_form"); ?>';
    </script>
    <?php
}

function my_htaccess_contents( $rules )
{
	return $rules . "\n" .
	       "<IfModule mod_headers.c>
 <FilesMatch \"\.(jpg|jpeg|png|gif|swf|JPG)$\">
 Header set Cache-Control \"max-age=4838400, public\"
 </FilesMatch>\n
 <FilesMatch \"\.(css|js)$\">
 Header set Cache-Control \"max-age=4838400, private\"
 </FilesMatch>\n
 </IfModule>";
}
add_filter('mod_rewrite_rules', 'my_htaccess_contents');


class Top_Menu_Walker extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        if (0 == $depth) {
            $current = null;
            $contact = null;
            if (array_search('current-menu-item', $item->classes) != 0) {
                $current = "-current";
            }
            if ($item->url == get_home_url()."/contact") {
                $contact = "-contact";
            }
            $item_html = apply_filters('the_title', $item->title, $item->ID);
            $output .= '<a href ="' . $item->url . '" class="menu__item '.$current. $contact.'" title="'.$item->title.'">' . $item_html . '</a>';
            return;
        }
    }
}

class Footer_Menu_Walker extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        if (0 == $depth) {
            $item_html = apply_filters('the_title', $item->title, $item->ID);
            $output .= '<li><a href ="' . $item->url . '" class="footer__link">' . $item_html . '</a></li>';
            return;
        }
    }
}