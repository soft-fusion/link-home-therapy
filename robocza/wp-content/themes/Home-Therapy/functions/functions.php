<?php
add_theme_support( 'post-thumbnails' );
if(! defined( 'ABSPATH' )) return;
function generateRandomString($length = 20)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGIJKLMNOPRSTUWYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }

    return $randomString;
}
function set_html_content_type(){
	global $boundary;
	return "multipart/alternative;boundary=$boundary";
}


function get_id_by_template_filename($filename)
{
	global $wpdb;
	return $wpdb->get_var("SELECT wp_pm.post_id FROM {$wpdb->posts} wp_p JOIN {$wpdb->postmeta} wp_pm ON wp_p.ID=wp_pm.post_id WHERE wp_p.post_type='page' AND wp_p.post_status='publish' AND meta_key='_wp_page_template' AND meta_value='$filename' ");
}

function print_r_e($var)
{
    echo "<pre>";
	echo htmlspecialchars(print_r($var,true));
    echo "</pre>";
}


function set_html_content()
{
    global $boundary;
    return "multipart/alternative;boundary=$boundary";
}

function sf_send_email($to, $content, $title, $file)
{
    global $boundary;
    $boundary = md5(uniqid(rand()));
    add_filter('wp_mail_content_type','set_html_content');
    $content_of_mail =
        "This is a multi-part message in MIME format.\r\n" .
        "--" . $boundary . "\r\n" .
        "Content-type: text/plain;charset=utf-8\r\n\r\n" .
        strip_tags(str_replace('<br>',"\r\n",$content)) . "\r\n".
        "--" . $boundary . "\r\n".
        "Content-type: text/html;charset=utf-8\r\n\r\n" .
        $content;
    if($file === 0)
    {
        $sent = wp_mail($to, $title, $content_of_mail);
    }
    else
    {
        $sent = wp_mail($to, $title, $content_of_mail,'',$file);
    }
    remove_filter('wp_mail_content_type', 'set_html_content');

    return $sent;

}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Footer settings',
        'menu_title'	=> 'Footer settings',
        'menu_slug' 	=> 'footer_settings',
        'capability'	=> 'edit_posts',
    ));

    acf_add_options_page(array(
        'page_title' 	=> 'E-mail settings',
        'menu_title'	=> 'E-mail settings',
        'menu_slug' 	=> 'E-mail settings',
        'capability'	=> 'edit_posts',
    ));

}

function my_acf_add_local_field_groups() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'my_acf_add_local_field_groups');

function sf_get_posts($args){
    $query = new WP_Query($args);
    return $query->posts;
}

