<?php
class SF_testimonials extends SF_abstract_post
{
    public static $name = 'Testimonials';
    public static $slug = 'testimonials';

    public function createPost() {
        $labels = array(
            'name' => _x(self::$name, 'tag'),
            'singular_name' => _x(self::$name, 'tag'),
            'add_new' => _x('Add Testimonials', 'tag'),
            'add_new_item' => _x('Add Testimonials', 'tag'),
            'edit_item' => _x('Edit Testimonials', 'tag'),
            'new_item' => _x('New Testimonials', 'tag'),
            'view_item' => _x('View Testimonials', 'tag'),
            'search_items' => _x('Search Testimonials', 'tag'),
            'not_found' => _x('Not found', 'tag'),
            'not_found_in_trash' => _x('Not found in trash', 'tag'),
            'parent_item_colon' => _x('Rodzic:', 'tag'),
            'menu_name' => _x(self::$name, 'tag'),
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => false,
            'supports' => array('title', 'editor'),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 36,
            'menu_icon' => 'dashicons-image-flip-horizontal',
            'show_in_nav_menus' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'has_archive' => false,
            'query_var' => false,
            'can_export' => true,
            'capability_type' => 'post'
        );

        register_post_type(self::$slug, $args);
    }
}
new SF_testimonials();