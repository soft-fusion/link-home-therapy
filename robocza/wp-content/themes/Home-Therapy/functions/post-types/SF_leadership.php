<?php
class SF_leadership extends SF_abstract_post
{
    public static $name = 'Leadership';
    public static $slug = 'leadership';

    public function createPost() {
        $labels = array(
            'name' => _x(self::$name, 'tag'),
            'singular_name' => _x(self::$name, 'tag'),
            'add_new' => _x('Add Leadership', 'tag'),
            'add_new_item' => _x('Add Leadership', 'tag'),
            'edit_item' => _x('Edit Leadership', 'tag'),
            'new_item' => _x('New Leadership', 'tag'),
            'view_item' => _x('View Leadership', 'tag'),
            'search_items' => _x('Search Leadership', 'tag'),
            'not_found' => _x('Not found', 'tag'),
            'not_found_in_trash' => _x('Not found in trash', 'tag'),
            'parent_item_colon' => _x('Rodzic:', 'tag'),
            'menu_name' => _x(self::$name, 'tag'),
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => false,
            'supports' => array('title', 'editor','thumbnail'),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 36,
            'menu_icon' => 'dashicons-admin-users',
            'show_in_nav_menus' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'has_archive' => false,
            'query_var' => false,
            'can_export' => true,
            'capability_type' => 'post'
        );

        register_post_type(self::$slug, $args);
    }
}
new SF_leadership();