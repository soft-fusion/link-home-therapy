<?php

require_once("thumbnails.inc.php");
require_once("admin-menu.php");
require_once("functions.php");

require_once("post-types/SF_post.php");
require_once("post-types/SF_testimonials.php");
require_once("post-types/SF_leadership.php");
require_once("ajax_function/contact_form.php");