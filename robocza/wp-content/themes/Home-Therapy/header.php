<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>" />
	<title><?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta name="title" content="Template">
    <meta name="keywords" content="Template keywords">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="preload" href="<?php echo TEMP_URI; ?>/assets/styles/main2.min.css" as="style">
    <link rel="stylesheet" href="<?php echo TEMP_URI; ?>/assets/styles/main2.min.css">
    <link rel="manifest" href="<?php echo TEMP_URI; ?>/assets/favicon/manifest.webmanifest">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo TEMP_URI; ?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo TEMP_URI; ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo TEMP_URI; ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo TEMP_URI; ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo TEMP_URI; ?>/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-288409316"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-288409316');
    </script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div style="overflow-x:hidden">
    <header class="header">
        <div class="header__container">
            <div class="header__logo">
                <a class="logo" href="<?php echo home_url(); ?>" title="Link Home Therapy">
                    <img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/logo.svg" alt="link home therapy logo">
                </a>
            </div>
            <div class="header__menu">
                <div class="menu">
                    <div class="menu__button">
                        <img loading="lazy" class="menu__open" src="<?php echo TEMP_URI; ?>/assets/images/menu-open.svg" alt="menu button">
                    </div>
                    <div class="menu__container">
                        <a class="menu__logoLink" href="<?php echo home_url(); ?>" title="Link Home Therapy">
                            <img loading="lazy" class="menu__logo" src="<?php echo TEMP_URI; ?>/assets/images/logoWhite.svg" alt="link home therapy white logo">
                        </a>
                            <?php
                            $args = array(
                                'theme_location' => 'top_menu',
                                'container' => false,
                                'items_wrap' =>'<div class="menu__menu">%3$s</div>',
                                'item_spacing' => true,
                                'walker' => new Top_Menu_Walker()
                            );
                            wp_nav_menu($args);

                            ?>
                        <div class="menu__socials">
                            <?php
                            while( have_rows('social_media', 'option') ) {
                                the_row();
                                $social_media_name = get_sub_field('name_social_media');
                                $social_media_url = get_sub_field('url_social_media');
                                $social_media = get_sub_field('icon');
                                $social_media = wp_get_attachment_image_url($social_media['ID'], 'Social');
                                if(!empty($social_media) && !empty($social_media_name)):
                                ?>
                                <a class="footer__icon" href="<?php echo $social_media_url ?>" title="<?php echo $social_media_name ?>"><img loading="lazy" src="<?php echo $social_media ?>" alt="<?php echo $social_media_name ?>"></a>
                            <?php
                                endif;
                            }
                            ?>
                        </div>
                        <a class="menu__buttonLink" href="#" title="Payment Portal">Payment Portal</a>
                        <img loading="lazy" class="menu__close" src="<?php echo TEMP_URI; ?>/assets/images/menu-close.svg" alt="close">
                    </div>
                </div>
            </div>
        </div>
    </header>