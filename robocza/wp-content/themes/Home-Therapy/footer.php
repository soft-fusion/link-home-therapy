<?php
$copyright = get_field('copyright', 'option');
$id = get_the_ID();
?>

<div class="footer">
    <div class="footer__container">
        <?php if((int)$id === 13){ ?>
        <div class="footer__decorator"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/decorator-dashes-green-m.svg" alt="decorator"></div>
       <?php
       }
        ?>
        <div class="footer__left">
            <div class="footer__logo -mobile"><a class="logo" href="<?php echo home_url(); ?>" title="Link Home Therapy"><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/logo-footer.svg" alt="logo footer"></a></div>
            <div class="footer__menu">
                <ul class="footer__list">
                    <li class="footer__listTitle">MENU</li>
                    <?php
                    $args = array(
                        'theme_location' => 'footer_menu',
                        'container' => false,
                        'item_spacing' => true,
                        'walker' => new Footer_Menu_Walker()
                    );
                    wp_nav_menu($args);

                    ?>
                </ul>
                <ul class="footer__list -desktop">
                    <li class="footer__listTitle">SOCIAL</li>
                    <?php
                    while( have_rows('social_media', 'option') ) {
                        the_row();
                        $social_media_name = get_sub_field('name_social_media');
                        $social_media_url = get_sub_field('url_social_media');
                        ?>
                        <li><a class="footer__link" href="<?php echo $social_media_url ?>" title="Fallow us on <?php echo $social_media_name ?>"><?php echo $social_media_name; ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="footer__copyright -desktop"><?php echo $copyright; ?></div>
        </div>
        <?php
        while( have_rows('other_link', 'option') ) {
        the_row();
        $other_name = get_sub_field('other_link_name');
        $other_url = get_sub_field('other_link_url');
        ?>
        <div class="footer__right"><a class="customButton" href="<?php echo $other_url; ?>" title="<?php echo $other_name; ?>"><?php echo $other_name; ?></a>
<?php } ?>
            <div class="footer__logo -desktop"><a class="logo" href="<?php echo home_url(); ?>" title="Link Home Therapy">
                    <img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/logo-footer.svg" alt="footer logo"></a>
            </div>
            <div class="footer__social">
                <?php
                while( have_rows('social_media', 'option') ) {
                    the_row();
                    $social_media_name = get_sub_field('name_social_media');
                    $social_media_url = get_sub_field('url_social_media');
                    $social_media = get_sub_field('icon');
                    $social_media = wp_get_attachment_image_url($social_media['ID'], 'Social');
                    if(!empty($social_media) && !empty($social_media_name)):
                    ?>
                    <a class="footer__icon" href="<?php echo $social_media_url ?>" title="<?php echo $social_media_name ?>"><img loading="lazy" src="<?php echo $social_media ?>" alt="social media"></a>
                <?php
                endif;
                }
                ?>
            </div>
            <div class="footer__copyright -mobile"><?php echo $copyright; ?></div>
        </div>
    </div>
</div>
</div>
<script async src="<?php echo TEMP_URI; ?>/assets/scripts/main3.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>