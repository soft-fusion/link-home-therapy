import 'babel-polyfill';
import $ from 'jquery';
import categoryGrid from './components/categoryGrid/categoryGrid';
import letsTalk from './components/letsTalk/letsTalk';
import narrowText from './components/narrowText/narrowText';
import menu from './components/menu/menu';
import peopleGrid from './components/peopleGrid/peopleGrid';
import testimonialsSection from './components/testimonialsSection/testimonialsSection';
import banner from './components/banner/banner';
import buttonSection from './components/buttonSection/buttonSection';
import textSection from './components/textSection/textSection';
import mapSection from './components/mapSection/mapSection';
import contactSection from './components/contactSection/contactSection';
import issuesGrid from './components/issuesGrid/issuesGrid';
import issuesGridItemNew from './components/issuesGridItemNew/issuesGridItemNew';
import partnerForm from './components/partnerForm/partnerForm';

$(document).ready(() => {
  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
  };
  $.fn.showAnim = function(animated = false) {
    $(this).addClass('visible');
    setTimeout(() => {
      if (animated) {
        $(this).addClass('animated');
      }
    }, 1000);
  };

  menu.init();
  categoryGrid.init();
  peopleGrid.init();
  letsTalk.init();
  narrowText.init();
  testimonialsSection.init();
  banner.init();
  buttonSection.init();
  textSection.init();
  mapSection.init();
  contactSection.init();
  issuesGrid.init();
  issuesGridItemNew.init();
  partnerForm.init();
});
