import $ from 'jquery';

const mapSection = {
  settings: {
    target: '.mapSection',
    title: '.mapSection__title',
    subtitle: '.mapSection__subtitle',
    text: '.mapSection__text',
    map: '.mapSection__mapPlace',
    icon: '.mapSection__item'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      subtitle: target.find(settings.subtitle),
      text: target.find(settings.text),
      map: target.find(settings.map),
      icon: target.find(settings.icon)
    };
  },
  bindEvents() {
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if ($(this.$target.map).length > 0 && $(this.$target.map).isInViewport()) {
      $(this.$target.map).showAnim(true);
    }
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if (
      $(this.$target.subtitle).length > 0 &&
      $(this.$target.subtitle).isInViewport()
    ) {
      $(this.$target.subtitle).showAnim();
    }
    if (
      $(this.$target.text).length > 0 &&
      $(this.$target.text).isInViewport()
    ) {
      $(this.$target.text).showAnim();
    }
    if ($(this.$target.icon).length > 0) {
      $.each(this.$target.icon, (index, item) => {
        if ($(item).isInViewport()) {
          $(item).showAnim();
        }
      });
    }
  }
};

export default mapSection;
