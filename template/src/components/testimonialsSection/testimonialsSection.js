import $ from 'jquery';
import 'slick-carousel';

const testimonialsSection = {
  settings: {
    target: '.testimonialsSection',
    title: '.testimonialsSection__title',
    text: '.testimonialsSection__text',
    testimonials: '.testimonialsSection__testimonials',
    slider: '.testimonialsSection__slider'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.initSlick();
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      text: target.find(settings.text),
      testimonials: target.find(settings.testimonials),
      slider: target.find(settings.slider)
    };
  },
  bindEvents() {
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if (
      $(this.$target.text).length > 0 &&
      $(this.$target.text).isInViewport()
    ) {
      $(this.$target.text).showAnim();
    }
    if (
      $(this.$target.testimonials).length > 0 &&
      $(this.$target.testimonials).isInViewport()
    ) {
      $(this.$target.testimonials).showAnim();
    }
  },
  initSlick() {
    $(this.$target.slider).on('init', () => {
      if ($(window).width() > 768) {
        $('.testimonialsItem').each((index, item) =>
          $(item).css({ opacity: 0.15, transform: '' })
        );
        $(this.$target.slider)
          .find('.slick-slide.slick-current .testimonialsItem')
          .css({ opacity: '', transform: 'scale(1.11)' });
      }
    });
    $(this.$target.slider).on('afterChange', () => {
      if ($(window).width() > 768) {
        $('.testimonialsItem').each((index, item) =>
          $(item).css({ opacity: 0.15, transform: '' })
        );
        $(this.$target.slider)
          .find('.slick-slide.slick-current .testimonialsItem')
          .css({ opacity: '', transform: 'scale(1.11)' });
      }
    });
    $(this.$target.slider).slick({
      slidesToShow: 1,
      infinite: false,
      arrows: true,
      prevArrow: $('.testimonialsSection__prev'),
      nextArrow: $('.testimonialsSection__next'),
      centerMode: true,
      centerPadding: '41px',
      mobileFirst: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            centerPadding: '150px',
            infinite: true,
            draggable: false
          }
        },
        {
          breakpoint: 969,
          settings: {
            slidesToShow: 1,
            centerPadding: '212px',
            infinite: true,
            draggable: false
          }
        },
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 1,
            centerPadding: '425px',
            infinite: true,
            draggable: false
          }
        },
        {
          breakpoint: 1600,
          settings: {
            slidesToShow: 1,
            centerPadding: '637px',
            infinite: true,
            draggable: false
          }
        },
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 1,
            centerPadding: '850px',
            infinite: true,
            draggable: false
          }
        }
      ]
    });
  }
};
export default testimonialsSection;
