import $ from 'jquery';

const banner = {
  settings: {
    target: '.banner',
    title: '.banner__title',
    text: '.banner__text',
    seal: '.banner__seal'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      text: target.find(settings.text),
      seal: target.find(settings.seal)
    };
  },
  bindEvents() {
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.seal).length > 0 &&
      $(this.$target.seal).isInViewport()
    ) {
      $(this.$target.seal).showAnim(true);
    }
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if (
      $(this.$target.text).length > 0 &&
      $(this.$target.text).isInViewport()
    ) {
      $(this.$target.text).showAnim();
    }
  }
};

export default banner;
