import $ from 'jquery';

const partnerForm = {
  settings: {
    target: '.partnerForm',
    button: '#button_send'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      console.log(this.$target.button);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      button: target.find(settings.button)
    };
  },
  bindEvents() {
    $(this.$target.button).on('click', this.sendMail.bind(this));
  },
  sendMail(e) {
    e.preventDefault();
    var fd = new FormData();
    fd.append('name', $('#name').val());
    fd.append('email', $('#email').val());
    fd.append('message', $('#message').val());
    fd.append('phone', $('#phone').val());
    fd.append('type_mail', $('#type_mail').val());
    fd.append('sent_email', $('#sent_email').val());
    var status = 1;
    if (
      fd.get('name') === '' ||
      fd.get('message') === '' ||
      fd.get('phone') === '' ||
      fd.get('email') === ''
    ) {
      status = 2;
      var message =
        '<div style=" padding: 15px; color: #ec2b2b; text-align: center; font-size: 20px;">' +
        'Please complete all fields ' +
        '</div>';
      $('#usermessage').append(message);
    }
    var why = $('#type_mail').val();
    if (why === 'file' && status === 1) {
      fd.append('send_file', $('input[name=send_file]')[0].files[0]);
      var fileInput = document.getElementById('send_file');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.pdf)$/i;
      var allowedExtensions2 = /(\.docx)$/i;
      var allowedExtensions3 = /(\.doc)$/i;
      if (
        !allowedExtensions.exec(filePath) &&
        !allowedExtensions2.exec(filePath) &&
        !allowedExtensions3.exec(filePath)
      ) {
        status = 2;
        var message =
          '<div style=" padding: 15px; color: #ec2b2b; text-align: center; font-size: 20px;">' +
          'Wrong file format! Add a pdf or docx type file' +
          '</div>';
        $('#usermessage').append(message);
      }
    }
    setTimeout(function() {
      $(`#refresh_email`).load(` #usermessage`);
    }, 4000);
    if (status === 1) {
      $.ajax({
        type: 'POST',
        data: fd,
        url: ajax_link2,
        processData: false,
        contentType: false,
        success: function(response) {
          var message =
            '<div style=" padding: 15px; color: #6CCA98; text-align: center; font-size: 20px;">' +
            'Message was sent' +
            '</div>';
          $('#usermessage').append(message);
          setTimeout(function() {
            $(`#form_refresh`).load(` #send_refresh`);
          }, 10);
        }
      });
    }
  }
};

$(document).ready(function() {
  $('input[type="file"]').change(function(e) {
    var geekss = e.target.files[0].name;
    $('#file-message').text(geekss);
  });
});

export default partnerForm;
