import $ from 'jquery';

const menu = {
  settings: {
    target: '.menu',
    container: '.menu__container',
    button: '.menu__button',
    buttonClose: '.menu__close'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      container: target.find(settings.container),
      button: target.find(settings.button),
      buttonClose: target.find(settings.buttonClose)
    };
  },
  bindEvents() {
    $(this.$target.button).on('click', this.openMenu.bind(this));
    $(this.$target.buttonClose).on('click', this.closeMenu.bind(this));
  },
  openMenu(e) {
    $(this.$target.container).addClass('-open');
    $('body').css('overflow-y', 'hidden');
  },
  closeMenu(e) {
    $(this.$target.container).removeClass('-open');
    $('body').css('overflow-y', '');
  }
};
export default menu;
