import $ from 'jquery';

const contactSection = {
  settings: {
    target: '.contactSection',
    title: '.contactSection__title',
    text: '.contactSection__text',
    contactData: '.contactSection__contactData',
    bottomText: '.contactSection__bottomText'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      text: target.find(settings.text),
      contactData: target.find(settings.contactData),
      bottomText: target.find(settings.bottomText)
    };
  },
  bindEvents() {
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if (
      $(this.$target.text).length > 0 &&
      $(this.$target.text).isInViewport()
    ) {
      $(this.$target.text).showAnim();
    }
    if ($(this.$target.contactData).length > 0) {
      $.each(this.$target.contactData, (index, item) => {
        if ($(item).isInViewport()) {
          $(item).showAnim();
        }
      });
    }
    if (
      $(this.$target.bottomText).length > 0 &&
      $(this.$target.bottomText).isInViewport()
    ) {
      $(this.$target.bottomText).showAnim(true);
    }
  }
};

export default contactSection;
