import $ from 'jquery';

const letsTalk = {
  settings: {
    target: '.letsTalk',
    title: '.letsTalk__title',
    button: '.customButton',
    trigger: '.letsTalk__backToTop'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      button: target.find(settings.button),
      trigger: target.find(settings.trigger)
    };
  },
  bindEvents() {
    $(this.$target.trigger).on('click', () => {
      $(window).scrollTop(0);
    });
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if (
      $(this.$target.button).length > 0 &&
      $(this.$target.button).isInViewport()
    ) {
      $(this.$target.button).showAnim();
    }
  }
};
export default letsTalk;
