import $ from 'jquery';

const issuesGridItemNew = {
  settings: {
    target: '.issuesGridItemNew'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target
    };
  },
  bindEvents() {
    $(this.$target.root).on('click', this.toggleItem.bind(this));
    $('.issuesGridItemNew__text').on('click', e =>
      e.stopImmediatePropagation()
    );
  },
  toggleItem(e) {
    const target = $(e.currentTarget);
    if (!$(target).hasClass('-open')) {
      $(this.$target.root).removeClass('-open');
      $('.issuesGridItemNew__text')
        .stop()
        .fadeOut(350);
      $(target).addClass('-open');
      $(target)
        .find('.issuesGridItemNew__text')
        .stop()
        .fadeIn(350);
    } else {
      $(this.$target.root).removeClass('-open');
      $('.issuesGridItemNew__text')
        .stop()
        .fadeOut(350);
    }
  }
};

export default issuesGridItemNew;
