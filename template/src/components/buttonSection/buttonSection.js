import $ from 'jquery';

const buttonSection = {
  settings: {
    target: '.buttonSection',
    title: '.buttonSection__title',
    button: '.customButton'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      button: target.find(settings.button)
    };
  },
  bindEvents() {
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if (
      $(this.$target.button).length > 0 &&
      $(this.$target.button).isInViewport()
    ) {
      $(this.$target.button).showAnim();
    }
  }
};

export default buttonSection;
