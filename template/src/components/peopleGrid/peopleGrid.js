import $ from 'jquery';
import 'slick-carousel';

const peopleGrid = {
  settings: {
    target: '.peopleGrid',
    title: '.peopleGrid__title',
    text: '.peopleGrid__text',
    slider: '.peopleGrid__slider',
    slick: '.peopleGrid__box'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.initSlick();
      this.setSectionHeight();
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      text: target.find(settings.text),
      slider: target.find(settings.slider),
      slick: target.find(settings.slick)
    };
  },
  bindEvents() {
    $(window).on('resize', () => {
      setTimeout(() => {
        this.setSectionHeight();
      }, 50);
    });
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if (
      $(this.$target.text).length > 0 &&
      $(this.$target.text).isInViewport()
    ) {
      $(this.$target.text).showAnim();
    }
    if (
      $(this.$target.slider).length > 0 &&
      $(this.$target.slider).isInViewport()
    ) {
      $(this.$target.slider).showAnim();
    }
  },
  setSectionHeight() {
    $('.peopleGrid__slider').css('height', '');
    $('.peopleGrid__slider').css('height', $('.peopleGrid__slider').height());
  },
  initSlick() {
    $(this.$target.slick).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: $('.peopleGrid__prev'),
      nextArrow: $('.peopleGrid__next'),
      infinite: true,
      centerMode: true,
      centerPadding: '50px',
      mobileFirst: true,
      responsive: [
        {
          breakpoint: 374,
          settings: {
            centerMode: true,
            centerPadding: '70px',
            draggable: false
          }
        },
        {
          breakpoint: 413,
          settings: {
            centerMode: true,
            centerPadding: '107px',
            draggable: false
          }
        },
        {
          breakpoint: 535,
          settings: {
            centerMode: true,
            centerPadding: '135px',
            draggable: false
          }
        },
        {
          breakpoint: 639,
          settings: {
            centerMode: true,
            centerPadding: '165px',
            draggable: false
          }
        },
        {
          breakpoint: 699,
          settings: {
            centerMode: true,
            centerPadding: '195px',
            draggable: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            centerMode: true,
            centerPadding: '100px',
            draggable: false
          }
        },
        {
          breakpoint: 1023,
          settings: {
            slidesToShow: 3,
            centerMode: true,
            centerPadding: '40px',
            draggable: false
          }
        },
        {
          breakpoint: 1279,
          settings: {
            slidesToShow: 3,
            centerMode: true,
            centerPadding: '150px',
            draggable: false
          }
        },
        {
          breakpoint: 1439,
          settings: {
            slidesToShow: 3,
            centerMode: true,
            centerPadding: '240px',
            draggable: false
          }
        },
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 3,
            centerMode: true,
            centerPadding: '400px',
            draggable: false
          }
        }
      ]
    });
  }
};
export default peopleGrid;
