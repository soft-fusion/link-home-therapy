import $ from 'jquery';
import 'slick-carousel';

const categoryGrid = {
  settings: {
    target: '.categoryGrid',
    title: '.categoryGrid__title',
    slider: '.categoryGrid__slider',
    slick: '.categoryGrid__box'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.initSlick();
      this.setHeight();
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      slider: target.find(settings.slider),
      slick: target.find(settings.slick)
    };
  },
  bindEvents() {
    $(window).on('resize', () => {
      if (
        $(window).width() < 769 &&
        !$(this.$target.slick).hasClass('slick-initialized')
      ) {
        this.initSlick();
      }
    });
    $(window).on('resize', () => {
      setTimeout(() => {
        this.setHeight();
      }, 50);
    });
    $(this.$target.slick).on('beforeChange', () => {
      this.setHeight();
    });
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim(true);
    }
    if (
      $(this.$target.slider).length > 0 &&
      $(this.$target.slider).isInViewport()
    ) {
      $(this.$target.slider).showAnim(true);
    }
  },
  setHeight() {
    let minHeight = 0;
    const titles = $(this.$target.slick).find('.categoryGridItem__title');
    titles.css('min-height', '');
    $.each(titles, (index, item) => {
      if ($(item).height() > minHeight) {
        minHeight = $(item).height();
      }
    });
    titles.css('min-height', `${minHeight}px`);
  },
  initSlick() {
    let slidesToShow = 3;
    let slidesNum = $(this.$target.slick).find('.categoryGridItem').length;
    $('.categoryGrid__prev, .categoryGrid__next').css('display', '');
    if ($(window).width() < 769 || slidesNum > slidesToShow) {
      $('.categoryGrid__prev, .categoryGrid__next').css('display', 'block');
      $(this.$target.slick).slick({
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '49px',
        arrows: true,
        prevArrow: $('.categoryGrid__prev'),
        nextArrow: $('.categoryGrid__next'),
        infinite: false,
        mobileFirst: true,
        responsive: [
          {
            breakpoint: 768,
            settings:
              slidesNum > slidesToShow
                ? {
                    slidesToShow,
                    centerMode: false,
                    draggable: false
                  }
                : 'unslick'
          }
        ]
      });
    }
  }
};
export default categoryGrid;
