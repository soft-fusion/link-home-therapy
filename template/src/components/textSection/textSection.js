import $ from 'jquery';

const textSection = {
  settings: {
    target: '.textSection',
    left: '.textSection__left',
    right: '.textSection__right',
    icon: '.textSection__item'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      left: target.find(settings.left),
      right: target.find(settings.right),
      icon: target.find(settings.icon)
    };
  },
  bindEvents() {
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.left).length > 0 &&
      $(this.$target.left).isInViewport()
    ) {
      $(this.$target.left).showAnim();
    }
    if (
      $(this.$target.right).length > 0 &&
      $(this.$target.right).isInViewport()
    ) {
      $(this.$target.right).showAnim();
    }
    if ($(this.$target.icon).length > 0) {
      $.each(this.$target.icon, (index, item) => {
        if ($(item).isInViewport()) {
          $(item).showAnim();
        }
      });
    }
  }
};

export default textSection;
