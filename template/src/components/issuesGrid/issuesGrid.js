import $ from 'jquery';

const issuesGrid = {
  settings: {
    target: '.issuesGrid',
    title: '.issuesGrid__title',
    item: '.issuesGridItemNew'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      item: target.find(settings.item)
    };
  },
  bindEvents() {
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if (
      $(this.$target.title).length > 0 &&
      $(this.$target.title).isInViewport()
    ) {
      $(this.$target.title).showAnim();
    }
    if ($(this.$target.item).length > 0) {
      $.each(this.$target.item, (index, item) => {
        if ($(item).isInViewport()) {
          $(item).showAnim();
        }
      });
    }
  }
};

export default issuesGrid;
