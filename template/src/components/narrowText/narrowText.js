import $ from 'jquery';

const narrowText = {
  settings: {
    target: '.narrowText',
    title: '.narrowText__title',
    text: '.narrowText__text',
    trigger: '.narrowText__backToTop'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      title: target.find(settings.title),
      text: target.find(settings.text),
      trigger: target.find(settings.trigger)
    };
  },
  bindEvents() {
    $(this.$target.trigger).on('click', () => {
      $(window).scrollTop(0);
    });
    $(() => {
      this.showElements();
    });
    $(window).on('scroll', () => {
      this.showElements();
    });
  },
  showElements() {
    if ($(this.$target.title).length > 0) {
      $.each(this.$target.title, (index, item) => {
        if ($(item).isInViewport()) {
          $(item).showAnim();
        }
      });
    }
    if (
      $(this.$target.text).length > 0 &&
      $(this.$target.text).isInViewport()
    ) {
      $(this.$target.text).showAnim();
    }
  }
};
export default narrowText;
