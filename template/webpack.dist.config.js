/* eslint-env node */
const path = require('path');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: './src/main.js',
  plugins: [
        new CaseSensitivePathsPlugin()
  ],
  output: {
    path: path.resolve(__dirname, 'build/assets/scripts'),
    publicPath: 'build/assets/scripts/',
    filename: 'main3.min.js'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};
